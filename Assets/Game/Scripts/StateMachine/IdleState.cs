using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IStateMachine
{
    float timer;
    float randomTimer;
    public void OnEnter(Enemy enemy)
    {
        timer = 0;
        randomTimer = Random.Range(1f, 3f);
        enemy.StopMove();
    }

    public void OnExcute(Enemy enemy)
    {
        timer += Time.deltaTime;
        if (enemy.GetCharacter())
        {
            enemy.ChangState(new AtackState());
        }
        if (timer > randomTimer)
        {
            enemy.ChangState(new PatrolState());
        }



    }

    public void OnExit(Enemy enemy)
    {

    }
}
