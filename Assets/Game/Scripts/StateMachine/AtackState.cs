using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtackState : IStateMachine
{
    float timer;
    float randomTime;
    public void OnEnter(Enemy enemy)
    {
        timer = 0;
        enemy.Atack();
    }
    public void OnExcute(Enemy enemy)
    {
        timer += Time.deltaTime;
        if (timer > 2f)
        {
            enemy.ChangState(new IdleState());
        }
    }

    public void OnExit(Enemy enemy)
    {

    }
}
