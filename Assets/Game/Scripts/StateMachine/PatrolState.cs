using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IStateMachine
{
    float timer;
    float randomTime;
    public void OnEnter(Enemy enemy)
    {
        timer = 0;
        randomTime = Random.Range(2f, 4f);
        //enemy.ChangAnim("run");
       
    }

    public void OnExcute(Enemy enemy)
    {
        timer += Time.deltaTime;
        if (enemy.GetCharacter())
        {
            enemy.ChangState(new IdleState());
        }
        else
        {
            if (timer < randomTime)
            {
                enemy.Move();
            }
        }

    }

    public void OnExit(Enemy enemy)
    {

    }
}
