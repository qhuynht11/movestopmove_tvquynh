using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveSystem : Singleton<SaveSystem>  
{
    public PlayerData level;

    public void SavePlayer()
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/huynhdeptrai";
        FileStream stream = new FileStream(path, FileMode.Create);
        Debug.Log(path);
        //PlayerData data = new PlayerData(level);
        binaryFormatter.Serialize(stream, level);
        stream.Close();
    }
    public PlayerData Load()
    {
        string path = Application.persistentDataPath + "/huynhdeptrai";
        if (File.Exists(path))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            PlayerData data = binaryFormatter.Deserialize(stream) as PlayerData;
            stream.Close();
            return data;
        }
        else
        {

            Debug.Log("Save not found files");
            return null;
        }
    }



}
