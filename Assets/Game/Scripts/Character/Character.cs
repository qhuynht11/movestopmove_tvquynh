using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] protected GameObject point_X1;
    [SerializeField] protected GameObject point_X2;
    [SerializeField] protected GameObject point_Z1;
    [SerializeField] protected GameObject point_Z2;
    [SerializeField] protected Animator animator;
    [SerializeField] private Weapon myWeapon;
    [SerializeField] private Atacker m_atack;
    [SerializeField] private WeaponType aaa;
    protected bool isDeath; 
    public bool IsDeath
    {
        get
        {
            return isDeath;
        }
        set
        {
            isDeath = value;
        }
    }
    protected string curentAnim;
    void Start()
    {
        OnInit();
    }

    // Update is called once per frame
    void Update()
    {
        OnUpdate();
    }
    private void FixedUpdate()
    {
        OnFixUpdate();
    }

    protected virtual void OnInit()
    {
       // ChangAnim("idle");
    }
    protected virtual void OnUpdate()
    {

    }
    protected virtual void OnFixUpdate()
    {

    }
    public virtual Character GetCharacter()
    {
        Character character = m_atack.GetCharacterIsRange();
        if (!character) return null;
        return character;
    }
    public virtual void Atack()
    {
        ChangAnim("atack");
        if (!GetCharacter()) return;
        GameObject weapon = MuitiPooling.Instance.GetGameObject(PoolType.Bullet);
        weapon.transform.position = transform.position + new Vector3(0, 2, 0);
        Vector3 dirc = GetCharacter().transform.position - transform.parent.position;
        weapon.GetComponent<Bullet>().Direction(dirc);
        // xu ly lien ket giua weapon va character de cong diem va scale
        IsDeath = myWeapon.IsTouch;
        Debug.Log(IsDeath);
        if (IsDeath)
        {

            ChangScore();
        }
    }
    protected void ChangScore()
    {
        float addScore = 0.5f;
        transform.parent.localScale = transform.parent.localScale + new Vector3(addScore, addScore, addScore);

    }
    public void ChangAnim(string animName)
    {
        if (curentAnim != animName)
        {
            animator.ResetTrigger(animName);
            curentAnim = animName;
            animator.SetTrigger(curentAnim);
        }
    }

}
