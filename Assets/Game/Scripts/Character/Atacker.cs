using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atacker : MonoBehaviour
{
    [SerializeField] private List<Character> charactersList;

    private void Awake()
    {
        charactersList = new List<Character>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Enemy"))
        {
            charactersList.Add(other.GetComponent<Character>());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Enemy"))
        {
            charactersList.Remove(other.GetComponent<Character>());
        }
    }

    public Character GetCharacterIsRange()
    {
        Character cha = new Character();
        float minDistace = float.MaxValue;
        foreach(Character character in charactersList)
        {
            float distance = Vector3.Distance(transform.position, character.transform.position);
            if (distance < minDistace)
            {
                minDistace = distance;
                cha = character;
            }
        }
        if (cha != null) return cha;
        return null; 
    }
}
