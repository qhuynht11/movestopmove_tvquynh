using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    [SerializeField] private Joystick joyStickPlayer; 
    [SerializeField] private float speed;
    [SerializeField] private CameraFollow camera;
    private Vector3 positionPlayer;
    

    protected override void OnInit()
    {
        base.OnInit();
        positionPlayer = transform.parent.position;
        camera.OnInit();
        
    }
    protected override void OnUpdate()
    {
        base.OnUpdate();
        MovePlayer();
    }
    protected override void OnFixUpdate()
    {
        base.OnFixUpdate();
       
    }
    private void LateUpdate()
    {
        camera.OnLateUpdade();
    }
    private void MovePlayer()
    {
        if (Input.GetMouseButton(0))
        {
            GetMove();
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (!GetCharacter())
            {
                ChangAnim("idle");
            }
            else
            {
                Atack();
            }
           
        }
    }
    private void GetMove()
    {
        if (positionPlayer.x < point_X1.transform.position.x || positionPlayer.x > point_X2.transform.position.x || positionPlayer.z < point_Z1.transform.position.z || positionPlayer.z > point_Z2.transform.position.z)
        {
            if(positionPlayer.x < point_X1.transform.position.x)
            {
                positionPlayer = new Vector3(point_X1.transform.position.x, positionPlayer.y, positionPlayer.z);
                transform.parent.position = positionPlayer;
            }
            else if(positionPlayer.x > point_X2.transform.position.x)
            {
                positionPlayer = new Vector3(point_X2.transform.position.x, positionPlayer.y, positionPlayer.z);
                transform.parent.position = positionPlayer;
            }
            else if (positionPlayer.z < point_Z1.transform.position.z)
            {
                positionPlayer = new Vector3(positionPlayer.x, positionPlayer.y, point_Z1.transform.position.z);
                transform.parent.position = positionPlayer;
            }
            else if (positionPlayer.z > point_Z2.transform.position.z)
            {
                positionPlayer = new Vector3(positionPlayer.x, positionPlayer.y, point_Z2.transform.position.z);
                transform.parent.position = positionPlayer;
            }
        }
        float x = joyStickPlayer.Horizontal;
        float z = joyStickPlayer.Vertical;

        positionPlayer = positionPlayer + new Vector3(x, positionPlayer.y, z) * speed * Time.fixedDeltaTime;
        transform.parent.position = positionPlayer;
        // get rotate direction from joystick to player
        float angle = Mathf.Atan2(x, z) * Mathf.Rad2Deg;
        // from joytick angle get direction rotate player in function euler
        transform.parent.rotation = Quaternion.Euler(0, angle, 0);
        ChangAnim("run");       
    }
}
