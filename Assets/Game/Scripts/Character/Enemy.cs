using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Character
{
    [SerializeField] protected NavMeshAgent meshAgent;
    protected IStateMachine curenState;

    protected override void OnInit()
    {
        base.OnInit();
        ChangState(new IdleState());
    }
    protected override void OnUpdate()
    {
        base.OnUpdate();
        StateExcute();
    }
    public void StateExcute()
    {
        if (curenState != null)
        {
            curenState.OnExcute(this);
        }
    }
    public void ChangState(IStateMachine newState)
    {
        if (curenState != newState && curenState != null)
        {
            curenState.OnExit(this);
        }
        curenState = newState;
        if (curenState != null)
        {
            curenState.OnEnter(this);
        }
    }
    //public void FindTargetMove()
    //{
    //    if (GetCharacter())
    //    {
           
    //        ChangState(new IdleState());
    //    }
    //    else
    //    {
          
    //        Move();
    //    }
    //}
    public void Move()
    {
        ChangAnim("run");
        float x = Random.Range(point_X1.transform.position.x, point_X2.transform.position.x);
        float z = Random.Range(point_Z1.transform.position.z, point_Z2.transform.position.z);
        Vector3 position = new Vector3(x, 0, z);
        meshAgent.SetDestination(position);
        meshAgent.isStopped = false;
   
    }
    public void StopMove()
    {
        ChangAnim("idle");
        meshAgent.isStopped = true;
    }
}
