using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] protected Vector3 Distance;
    [SerializeField] protected Transform m_player;

    public void OnInit()
    {
        Distance = m_player.transform.position - transform.position;
    }
    public void OnLateUpdade()
    {
        //Distance = m_player.transform.position - transform.position;
        transform.position = m_player.transform.position - Distance;
    }
}