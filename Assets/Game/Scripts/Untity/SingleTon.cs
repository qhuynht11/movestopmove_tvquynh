using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component
{
    private static T instance;
    public static T Instance
    {
        get { return GetInstance(); }
    }

    public static T GetInstance()
    {
        instance = FindObjectOfType<T>();
        if(instance == null)
        {
            GameObject gb = new GameObject("Singleton");
            instance = gb.AddComponent<T>();
            DontDestroyOnLoad(gb);
        }
        return instance;
    }
}
