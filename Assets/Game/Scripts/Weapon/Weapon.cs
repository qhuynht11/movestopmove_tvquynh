using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
    bullet,
    bomerang
}
public class Weapon : MonoBehaviour
{
    [SerializeField] protected GameObject weponFrefab;
    [SerializeField] protected Rigidbody rb;
    [SerializeField] protected float speed;
    [SerializeField] public float score;
    [SerializeField] protected Character characterOwner;
    protected bool isTouch;
    public bool IsTouch
    {
        get
        {
            return isTouch;
        }
        set
        {
            isTouch = value;
        }
    }
    protected Vector3 direction;
    private void OnEnable()
    {
        
    }
    void Start()
    {
        OnInit();
    }

    // Update is called once per frame
    void Update()
    {
        OnUpdate();
    }
    private void FixedUpdate()
    {
        OnFixUpdate();
     
    }
    protected virtual void OnInit()
    {
        rb = GetComponent<Rigidbody>();
        score = 0;
        IsTouch = false;
    }
    protected virtual void OnUpdate()
    {

    }
    protected virtual void OnFixUpdate()
    {
        Move();
    }
    protected virtual void Move()
    {

    }
    public virtual void Direction(Vector3 dir)
    {
        direction = dir;
    }
    protected virtual void OnExit()
    {
        
    }
    protected virtual void IsTouchCharacter()
    {
        score++;
        Debug.Log(score);
        IsTouch = true;
        Debug.Log(IsTouch);
    }
    private void OnTriggerEnter(Collider other)
    {
        //ToDo
        //if (/*other.CompareTag("Player") ||*/ /*other.CompareTag("Enemy")*/)
        //{
           //IsTouchCharacter();     
        //}
    }
}
