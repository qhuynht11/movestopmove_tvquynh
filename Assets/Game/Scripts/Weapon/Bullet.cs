using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Weapon
{

    [SerializeField] private GameObject endBullet;
    //protected Transform myTransform;
    private void OnEnable()
    {
        //Invoke(nameof(OnExit), 2f);
    }
    protected override void OnInit()
    {
        base.OnInit();
        //myTransform =  transform.GetComponent<Transform>();
    }
    protected override void OnFixUpdate()
    {
        base.OnFixUpdate();
        OnExit();
        
    }
    protected override void Move()
    {
        base.Move();
        //rb.velocity = direction * speed * Time.fixedDeltaTime;
        //transform.position = transform.position+ myTransform.Translate()
        transform.Translate(direction * speed * Time.fixedDeltaTime);
    }
    protected override void OnExit()
    {

        base.OnExit();
        float distance = Vector3.Distance(endBullet.transform.position, transform.position);
        Debug.Log(distance);
        if (distance < 0.1f)
        {
            MuitiPooling.Instance.ReturnGameObject(gameObject, PoolType.Bullet);
        }
       // MuitiPooling.Instance.ReturnGameObject(gameObject, PoolType.Bullet);
    }
    protected override void IsTouchCharacter()
    {
        base.IsTouchCharacter();
        MuitiPooling.Instance.ReturnGameObject(gameObject, PoolType.Bullet);
    }

}
